# Test technique PALO IT

## Présentation

**Type de projet** : Test technique

**Présentation** :
	Le projet est l'infrastructure back-end d’une application mettant à disposition des données.

**Sujet** :
  Le sujet (fichier au format d'origine docx et en pdf) et les sources de données sont dans le dossier [subject](subject).

**Les services** :
  Les services sont conteneurisés avec Docker et orchestré avec Docker Compose. Les sources des services sont dans le dossier [services](services).

**Le provisioning** :
  Le déploiement des ressources Cloud et des services se fait avec Terraform sur Google Cloud Platform. Les sources de la configuration sont dans sont dans le dossier [provisioning/terraform](provisioning/terraform).

## Stack technique du projet

![Stack technique du projet](docs/schemas/tech-stack.png "Stack technique du projet")

## Autre

**Documentation** :
    La documentation de base est dans le dossier [docs](docs). D'autre dossiers de documentation avec le nom de `docs/` sont present dans le projet ([provisioning/terraform/docs](provisioning/terraform/docs) et [services/docs](services/docs)).

**Auteurs** :
* **Charles Garoux** (développeur)
