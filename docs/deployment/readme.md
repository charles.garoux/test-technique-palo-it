# Déploiement

**Méthode** : manuel (par CLI)

**Outil d'Infrastructure as Code'** : Terraform

**Fournisseur Cloud** : Google Cloud Platform

**Documentation du déploiement** : [provisioning/terraform/readme.md](provisioning/terraform/readme.md)

**Appliquer une modification** : [provisioning/terraform/docs/update.md](provisioning/terraform/docs/update.md)
