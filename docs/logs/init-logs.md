# Initialisation des logs dans Kibana

**Pré-requis** :
    Des logs des services concernés doivent avoir été gérés avant de pouvoir créer leurs paterne d’index Kibana.

## 1 - **Accéder à la création des paterne d'index sur Kibana** :
```
http://KIBANA_HOST:KIBANA_PORT/app/management/kibana/indexPatterns/create
```
> KIBANA_HOST = Adresse du service Kibana
>
> KIBANA_PORT = Port du service Kibana

![Screenshot du formulaire de création de paterne d'index Kibana](screenshots/kibana-create-index-pattern-screenshot.JPG "Screenshot du formulaire de création de paterne d'index Kibana")

## 2 - **Créer le paterne d'index des logs du service "tool" :**
![Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant tool-logs](screenshots/tool-logs-kibana-create-index-pattern-screenshot.JPG "Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant tool-logs")
> Sélectionner "Next step" pour valider

![Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant le "time field"](screenshots/kibana-create-index-pattern-screenshot-2.JPG "Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant le champ de temps")
> Sélectionner "Create index pattern" pour valider

## 3 - **Créer le paterne d'index des logs du service "api" :**
![Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant api-logs](screenshots/api-logs-kibana-create-index-pattern-screenshot.JPG "Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant api-logs")
> Sélectionner "Next step" pour valider

![Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant le "time field"](screenshots/kibana-create-index-pattern-screenshot-2.JPG "Screenshot du formulaire de création de paterne d'index Kibana en sélectionnant le champ de temps")
> Sélectionner "Create index pattern" pour valider