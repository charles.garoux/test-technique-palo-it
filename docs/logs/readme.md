# Surveillance des logs

**Présentation** :
    L’observation des logs des applications (les services "api" et "tool") se fait via l’application web Kibana.

**Initialisation des logs dans Kibana** : [init-logs.md](init-logs.md)

**Visualiser les logs** :
```
http://KIBANA_HOST:KIBANA_PORT/app/discover
```
> KIBANA_HOST = Adresse du service Kibana
>
> KIBANA_PORT = Port du service Kibana