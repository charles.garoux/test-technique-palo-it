# Documentation

## Schéma de fonctionnement du project

**Schéma des services du projet avec le service API** (Docker Compose) :

![Schéma des services du projet avec le service API](schemas/services-stack-for-api-service.png "Schéma des services du projet avec le service API")

**Schéma des services du projet avec le service Tool** (Docker Compose) :

![Schéma des services du projet avec le service Tool](schemas/services-stack-for-tool-service.png "Schéma des services du projet avec le service Tool")

> **A noter pour les 2 "Schéma des services du projet"** : à part les services "API" et "tool" avec leurs volumes Docker,
> les autres éléments visibles sur le schéma sont les mêmes (base de données, Logstash, Elasticsearch et Kibana).
> Il n’y a pas de doublon des services, cette duplication visuelle des schémas est faite pour plus de clarté de lecture.

**Schéma de l'infrastructure Cloud du projet** (GCP) :

![Schéma de l'infrastructure Cloud du projet](schemas/cloud-stack.png "Schéma de l'infrastructure Cloud du projet")

## Les dossiers

### schemas/
**Description** :
    Contient les schémas du projet.

### schemas/drawio-src/
**Description** :
    Contient les sources draw.io des schémas du projet.

### api/
**Description** :
    Contient les informations d'exploitation de l’API pour le client.

### postman/
**Description** :
    Contient la collection postman pour les services.

### deployment/
**Description** :
    Contient les informations pour le déploiement.
    
### logs/
**Description** :
    Contient les informations pour les logs.