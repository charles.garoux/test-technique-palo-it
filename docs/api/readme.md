# API

**Protocol de communication** : HTTP

**Port** : 3000

**Authentification** : aucune authentification nécessaire

**Mode** : lecture seul

**Format de réponse** : les données sont au format JSON

**Adresse de la documentation Swagger de l’API** : 
```
http://API_HOST:API_PORT/doc
```
> API_HOST = Adresse du service API
>
> API_PORT = Port du service API

# API Elasticsearch

**Port** : 9200