#!/usr/bin/env bash

# Description : docker exec on "tool" service with STDIN open
# Script usage : bash tool-exec.bash COMMAND [ARG...]

docker exec -i $(docker ps | grep "tool" | cut -d " " -f1) "$@"