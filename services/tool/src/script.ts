/**
 * SETUP
 */

import * as fs from 'fs';
import * as csvParser from 'csv-parser';
import * as scramjet from 'scramjet';
import {generateFieldBindingFunction} from './lib/process-data';
import {generateInsertOrUpdateDealerFunction} from './lib/insert-data';
import DB from './lib/db';
import Logger from './lib/logger';
import {generateCheckCanBeProcessedFilter} from './lib/filter-data';

/**
 * PARSING INPUT
 */

if (process.argv.length < 5) {
    Logger.script.error('Argument(s) missing');
    process.exit(2);
}
const [sourceFile, destinationTableName, idFieldName, csvSeparator = ',']: string[] = process.argv.splice(2);
Logger.script.debug(`Arguments`, {rawData: {sourceFile, destinationTableName, idFieldName, csvSeparator}});

const dbAccess: { host: string, user: string, password: string, database: string, poolTimeout: number, connectionTimeout: number } = {
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'toto',
    password: process.env.DB_PASSWORD || '1234',
    database: process.env.DB_DATABASE || 'example',
    poolTimeout: parseInt(process.env.DB_CLIENT_POOL_TIMEOUT || '10000', 10),
    connectionTimeout: parseInt(process.env.DB_CLIENT_CONNECTION_TIMEOUT || '10000', 10)
};

const pathToCsv = `${process.env.INPUT_DIRECTORY || ''}${sourceFile}`;

/**
 * SCRIPT
 */

DB.init(dbAccess.host, dbAccess.user, dbAccess.password, dbAccess.database, dbAccess.poolTimeout, dbAccess.connectionTimeout)
    .then(() => {
        Logger.script.info('CSV processing started');

        const readStream = (sourceFile === 'STDIN') ?
            scramjet.StringStream.from(process.stdin) :
            fs.createReadStream(pathToCsv);

        const csvDataStream = readStream.pipe(csvParser({separator: csvSeparator}));

        const stream = scramjet.DataStream.from(csvDataStream)
            .filter(generateCheckCanBeProcessedFilter(idFieldName))
            .map(generateFieldBindingFunction(idFieldName, 'id'))
            .each(generateInsertOrUpdateDealerFunction(destinationTableName))
            .catch((error: Error) => {
                const errorDescription = 'Error in stream';
                Logger.script.error(error, {errorDescription});
            });

        return stream.run();
    })
    .then(async () => {
        Logger.script.info('CSV processing finished');
        await DB.end();
    })
    .catch(async (error) => {
        const errorDescription = 'Can\'t finish script';
        Logger.script.error(error, {errorDescription});

        await DB.end();
        process.exit(1);
    });

