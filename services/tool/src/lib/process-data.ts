import Logger from './logger';

export function generateFieldBindingFunction(sourceFieldName: string, destinationFieldName: string = 'id') {
    return (data: any) => {
        if (!data.hasOwnProperty(sourceFieldName)) {
            const errorDescription = `Can\'t find "${sourceFieldName}" field`;
            Logger.other.error(errorDescription, {rawData: {sourceFieldName, destinationFieldName}});
            throw Error(errorDescription);
        }
        const tmpSourceField: any = data[sourceFieldName];
        delete data[sourceFieldName];
        data[destinationFieldName] = tmpSourceField;
        return data;
    };
}