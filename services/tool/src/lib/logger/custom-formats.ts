import * as winston from 'winston';
import {AllMetadataTemplateType} from './data-structures';
import * as chalk from 'chalk';
import {toPrettyJson} from './json';
import {EOL} from 'os';

export const customPrintFormat = winston.format.printf(
    ({level, rawLevel, message, element, module, rawData, stack, controller, suspectedFunction, errorDescription, requestId, method, url, responseStatus, responseLength, responseTime, ...info}
         : winston.Logform.TransformableInfo & AllMetadataTemplateType) => {
        // Common data
        const requestIdFormat = (requestId !== undefined) ? ` [${requestId}]` : '';
        const elementFormat = (element !== undefined) ? ` [${chalk.magenta(element)}]` : '';
        const moduleFormat = (module !== undefined) ? ` [module: ${module.name}]` : '';
        const rawDataFormat = (rawData !== undefined) ? ` [raw data JSON : ${toPrettyJson(rawData)}]` : '';

        const startFormat = `[${level}]${requestIdFormat}${elementFormat}${moduleFormat}`;
        const endFormat = `${rawDataFormat}`;

        if (rawLevel === 'error') {
            // Error data
            const suspectedFunctionFormat = (suspectedFunction !== undefined) ? ` [suspected function : ${suspectedFunction.name}]` : '';
            const errorDescriptionFormat = (errorDescription !== undefined) ? ` ${errorDescription} :${EOL}` : '';

            return `${startFormat} ${errorDescriptionFormat} ${stack || message}${suspectedFunctionFormat}${endFormat}`;
        }

        return `${startFormat} ${message}${endFormat}`;
    });

/**
 * Use to save "level" string before colorize log info
 */
export const saveRawLevelStringFormat = winston.format((info: winston.Logform.TransformableInfo & AllMetadataTemplateType, opts) => {
    info.rawLevel = info.level;

    return info;
});

/**
 * Use to add timestamp in metadata
 */
export const addDateFormat = winston.format((info: winston.Logform.TransformableInfo & AllMetadataTemplateType, opts) => {
    info.timestamp = Date.now();

    return info;
});