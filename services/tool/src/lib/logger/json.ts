const colorizeJson = require('json-colorizer');

const colorConfig = {
    'BRACE': 'white',
    'BRACKET': 'white',
    'COLON': 'white',
    'COMMA': 'white',
    'STRING_KEY': 'green',
    'STRING_LITERAL': 'green',
    'NUMBER_LITERAL': 'yellow',
    'BOOLEAN_LITERAL': 'yellow',
    'NULL_LITERAL': 'red'
};

export function toPrettyJson(data: any) {
    return colorizeJson(data, {
        pretty: true,
        colors: colorConfig
    });
}
