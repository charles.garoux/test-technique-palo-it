import Logger from './logger';

/**
 * Generate function to check the row fit the minimum criteria for the processing to work
 *
 * @param idFieldName
 */
export function generateCheckCanBeProcessedFilter(idFieldName: string) {
    return (data: object) => {
        const dataKeys: string[] = Object.keys(data);
        if (dataKeys.length > 0 &&
            dataKeys.includes(idFieldName))
            return true;
        else {
            Logger.other.warn('Invalid data filtered', {rawData: data});
            return false;
        }
    };
}