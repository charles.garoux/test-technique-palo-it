import * as mariadb from 'mariadb';
import {QueryOptions} from 'mariadb';
import Logger from './logger';

/**
 * Must be init (see "init()") before use
 */
export default class DB {
    static pool: mariadb.Pool;

    /**
     * Create connection pool to DB
     * @param host Database server host
     * @param user User username
     * @param password User password
     * @param database Database name
     * @param poolTimeout Get pool connection timeout
     * @param connectionTimeout Get connection timeout
     */
    static async init(host: string, user: string, password: string, database: string, poolTimeout: number = 10000, connectionTimeout: number = 10000) {
        if (Logger.dbClient.level === 'debug')
            Logger.dbClient.debug('Start init DB client',
                {rawData: {host, user, password, database, poolTimeout, connectionTimeout}});
        else
            Logger.dbClient.verbose('Start init DB client');

        this.pool = mariadb.createPool({
            host, user, password, database,
            acquireTimeout: poolTimeout,
            initializationTimeout: connectionTimeout
        });

        /**
         * Check if can get connection
         */

        try {
            const dbConnection = await this.pool.getConnection();
            await dbConnection.ping();
            await dbConnection.release();
        } catch (error) {
            const errorDescription = 'Can\'t ping DB after creating connection pool';
            Logger.dbClient.error(error,
                {errorDescription, rawData: {host, user, /* password, */ database, poolTimeout, connectionTimeout}});
            throw Error(errorDescription);
        }

        Logger.dbClient.verbose('Initialising DB client successfully');
    }

    /**
     * Close all connections in pool
     */
    static async end() {
        Logger.dbClient.verbose('Close connection to DB');
        return this.pool.end();
    }

    static async getConnection() {
        try {
            return this.pool.getConnection();
        } catch (error) {
            const errorDescription = 'Can\'t get pool connection';
            Logger.dbClient.error(error, {errorDescription});
            throw Error(errorDescription);
        }
    }

    static async query<TableElementType>(sqlQuery: string | QueryOptions, values?: any): Promise<TableElementType[]> {
        let dbConnection: mariadb.PoolConnection;

        try {
            dbConnection = await this.getConnection();
        } catch (error) {
            const errorDescription = 'Can\'t query DB because getting connection failed';
            Logger.dbClient.error(error, {errorDescription, rawData: {sqlQuery, values}});
            throw Error(errorDescription);
        }

        let data: TableElementType[];
        try {
            data = await dbConnection.query(sqlQuery, values);
            Logger.dbClient.debug(`Query "${sqlQuery}"`, {rawData: {sqlQuery, values, data}});
        } catch (error) {
            await dbConnection.release();

            const errorDescription = 'Can\'t query DB';
            Logger.dbClient.error(error, {errorDescription, rawData: {sqlQuery, values}});
            throw Error(errorDescription);
        }
        await dbConnection.release();

        return data;
    }
}
