import Logger from './logger';
import DB from './db';

/**
 * Generate function to insert or update data in a DB table
 *
 * Logic :
 *      if id not exit : insert new element,
 *      else : update element
 *
 * Warning : the generated function don't check if data is correct, if the database rejects the request : an error is throw
 *
 * @param destinationTableName
 */
export function generateInsertOrUpdateDealerFunction(destinationTableName: string) {
    Logger.other.verbose('Generate "insert or update" function');
    return async (data: object) => {
        const dataKeys = Object.keys(data);
        const dataValue = Object.values(data);

        const columnField = dataKeys
            .reduce((str, key) => `${str} \`${key}\`,`, '') // Generate column names field
            .slice(0, -1);

        const insertValueField = dataKeys.reduce((reducer) => `${reducer} ?,`, '') // Generate "VALUE" field
            .slice(0, -1);

        const updateField = dataKeys
            .reduce((str, key) => `${str} ${key} = ?,`, '') // Generate "UPDATE" field
            .slice(0, -1);

        const sqlQuery = `INSERT INTO ${destinationTableName} (${columnField}) VALUE (${insertValueField}) ON DUPLICATE KEY UPDATE ${updateField}`;
        const sqlQueryValues = dataValue.concat(dataValue);

        try {
            await DB.query(sqlQuery, sqlQueryValues);
            Logger.other.verbose(`Data inserted or updated`, {rawData: {data}});
        } catch (error) {
            const errorDescription = `Can\'t insert or update data in ${destinationTableName}`;
            Logger.other.error(error, {errorDescription, rawData: {data, sqlQuery, sqlQueryValues}});
            throw Error(errorDescription);
        }
    };
}