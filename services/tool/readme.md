# Tool

**Fonctionnement** :
	Le CSV est lu et pour chaque élément, s’il n’existe pas en base de données il l’ajoute, sinon il le met à jour.

**Dynamique** :
    Le programme génère de façon dynamique l'ajout/modification des éléments en base de données à partir des colonnes du CSV.
Cela permet qu’en cas de modification d'une table ou d’ajout d’une autre table, le programme ne nécessite pas de modification.

**Avertissement de sécurité** :
	L’aspect "dynamique" du programme peut le rendre faillible à travers les entrées utilisateur. Il est recommandé qu’il soit accessible seulement par des utilisateurs de confiance.

## Commande

**Liste des arguments du script** :
1. Chemin local vers le CSV, valeur special :
    * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
2. Nom de table en BDD
3. Nom de la colonne qui contient l'identifiant unique de l'element
4. Séparateur du CSV

**Installer les dépendances** :
```shell script
npm install
```

**Utiliser le service `tool`** :
```shell script
npm start PATH_TO_CSV TABLE_NAME COLUMN_ID_NAME CSV_SEPARATOR
```
> EXEC_ENV = environnement d'exécution (possibilité : "**prod**" et "**dev**")
>
> PATH_TO_CSV = chemin local vers le CSV ou valeur special ;
> * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
>
> TABLE_NAME = nom de table en BDD
>
> COLUMN_ID_NAME = nom de la colonne qui contient l'identifiant unique de l'élément
>
> CSV_SEPARATOR = séparateur du CSV (exemples : "**,**", "**;**", ...)

**Utiliser avec lecture sur STDIN** :
```shell script
npm start STDIN TABLE_NAME COLUMN_ID_NAME CSV_SEPARATOR < PATH_TO_CSV
```
> PATH_TO_CSV = chemin local vers le CSV ou valeur special ;
> * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
>
> TABLE_NAME = nom de table en BDD
>
> COLUMN_ID_NAME = nom de la colonne qui contient l'identifiant unique de l'élément
>
> CSV_SEPARATOR = séparateur du CSV (exemples : "**,**", "**;**", ...)

### CSV sur sur l'entrée standard (stdin)

**Recommandation d’utilisation** :
	Il est recommandé d’entrer le CSV sur l'entrée standard à travers une commande utilisant `<`, plutôt que par un copier-coller.

## Variables d'environnements

### Base de données

* **DB_HOST** = Adresse de l'hôte du serveur MariaDB
* **DB_USER** = Nom de l'utilisateur de l'API pour accéder à la base de données
* **DB_PASSWORD** = Mot de passe de l'utilisateur de l'API pour accéder à la base de données
* **DB_DATABASE** = Nom de la base de données de l'API sur le serveur MariaDB
* **DB_CLIENT_POOL_TIMEOUT** = Temps d'attente (en millisecondes) avant l'abandon de l'acquisition d'une "pool" de connexion avec le serveur MariaDB
* **DB_CLIENT_CONNECTION_TIMEOUT** = Temps d'attente (en millisecondes) avant l'abandon d'initialisation d'une connexion au serveur MariaDB

### Logs

* **LOG_LEVEL** = Niveau d'affichage des logs
(Possibilités dans l'ordre d'importance : `error`,`warn`,`info`,`http`,`verbose`,`debug` ou `silly`)
(Voir les niveaux de log de **Winston**: https://www.npmjs.com/package/winston#logging-levels)

* **LOG_DIRECTORY** = Dossier où seront écrit les fichiers de logs

## Autre

**Stack technique** :
* Node.js 16.x (LTS)
* Typescript
* Docker