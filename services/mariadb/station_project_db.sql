-- Adminer 4.8.1 MySQL 5.5.5-10.6.5-MariaDB-1:10.6.5+maria~focal dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

# CREATE DATABASE `station_project` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `station_project`;

DROP TABLE IF EXISTS `dealers`;
CREATE TABLE `dealers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modification_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `insertion_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(30) NOT NULL,
  `dealer_type` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `stations`;
CREATE TABLE `stations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modification_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `insertion_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `country_code` varchar(2) NOT NULL,
  `station_name` varchar(50) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `address` varchar(80) NOT NULL,
  `postal_code` varchar(5) NOT NULL,
  `city` varchar(40) NOT NULL,
  `dealer_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `dealer_id` (`dealer_id`),
  CONSTRAINT `stations_ibfk_1` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2021-11-28 16:32:23