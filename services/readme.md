# Source des services du projet

**Important à noter** :
* Les commandes nécessitent d'être exécuté depuis le même dossier que ce document (sauf indication contraire).

**Les commandes** : [docs/commands-usage.md](docs/commands-usage.md)

**Utilisation en environnement de Prod** : [docs/prod-commands.md](docs/prod-commands.md)

**Utilisation en environnement de développement** : [docs/dev-commands.md](docs/dev-commands.md)
