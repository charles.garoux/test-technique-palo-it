#!/usr/bin/env bash

EXEC_ENV=${1:-"prod"}
COMMAND=${2:-"up"}
FLAGS=${@:3}

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v "${PWD}:${PWD}" -w="${PWD}" --name docker-compose docker/compose:1.29.2 \
  --env-file "${DIR}/docker-compose.env" -p chgaroux-tech-test -f "${DIR}/docker-compose.base.yaml" -f "${DIR}/docker-compose.${EXEC_ENV}.yaml" "${COMMAND}" $FLAGS
