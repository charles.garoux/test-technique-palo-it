const http = require('http');

const options = {
    host: process.env.API_HOST || 'localhost',
    port: process.env.API_PORT || '3000',
    timeout: parseInt(process.env.TIMEOUT || "0", 10) || 3000,
    path: '/healthcheck'
};

const request = http.request(options, (res) => {
    console.log(`STATUS: ${res.statusCode}`);
    if (res.statusCode == 200)
        process.exit(0);
    else
        process.exit(1);
});

request.on('error', function (err) {
    console.error('Error with HTTP client :', err);
    process.exit(1);
});

request.end();
