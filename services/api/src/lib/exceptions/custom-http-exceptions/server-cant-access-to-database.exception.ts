import {HttpException} from '@nestjs/common';
import {CustomHttpStatus} from './custom-http-status';

export class ServerCantAccessToDatabase extends HttpException {
    constructor() {
        super('Server can\'t access to database', CustomHttpStatus.SERVER_CANT_ACCESS_TO_DATABASE);
    }
}
