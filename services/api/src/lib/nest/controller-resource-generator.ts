import {Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe} from '@nestjs/common';
import DB from '../db';
import {filterElementFromTableBase} from '../db-utils';
import {TableCommonToOmitType} from '../../data-structures/db/table-common-base.interface';
import Logger from '../logger';
import {ApiNotFoundResponse, ApiOkResponse, ApiTags} from '@nestjs/swagger';

type Class = Function

export default function generateResourceController<TableDataStructureType>(tableName: string, dtoClass: Class): any {
    @Controller(`${tableName}`)
    @ApiTags(`${tableName}`)
    class TableBaseController {

        @Get()
        @ApiOkResponse({
            description: `List of all ${dtoClass.name}`,
            type: dtoClass,
            isArray: true
        })
        async getAll(): Promise<Omit<TableDataStructureType, TableCommonToOmitType>[]> {
            let elements: TableDataStructureType[];

            try {
                elements = await DB.query<TableDataStructureType>(`SELECT * FROM ${tableName}`);
            } catch (error) {
                const errorDescription = `Can't get elements from "${tableName}" table`;
                Logger.api.error(error, {errorDescription});
                throw new HttpException(errorDescription, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            return elements.map((stationFromDB) => filterElementFromTableBase(stationFromDB));
        }

        @Get(`:id`)
        @ApiOkResponse({
            description: `${dtoClass.name} selected by id`,
            type: dtoClass
        })
        @ApiNotFoundResponse({
            description: `${dtoClass.name} not found by id`
        })
        async getOne(@Param('id', ParseIntPipe) id: number): Promise<Omit<TableDataStructureType, TableCommonToOmitType>> {
            let element: TableDataStructureType;

            try {
                element = (await DB.query<TableDataStructureType>(`SELECT * FROM ${tableName} WHERE id = ? LIMIT 1`, [id]))[0];
            } catch (error) {
                const errorDescription = `Can't get element from "${tableName}" table`;
                Logger.api.error(error, {errorDescription});
                throw new HttpException(errorDescription, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            if (element === undefined) throw new HttpException('Not found', HttpStatus.NOT_FOUND);

            return filterElementFromTableBase(element);
        }
    }

    return TableBaseController;
}
