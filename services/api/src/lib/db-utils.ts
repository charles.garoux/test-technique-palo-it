import TableCommonBaseInterface, {TableCommonToOmitType} from '../data-structures/db/table-common-base.interface';

/**
 * WARNING : "element" parameter must be a child object of TableCommonBaseInterface
 * @param element Child object of TableCommonBaseInterface
 *
 */
export function filterElementFromTableBase<TableType = TableCommonBaseInterface>(element: TableType) {
    // @ts-ignore
    delete element.modification_time;
    // @ts-ignore
    delete element.insertion_time;

    return element as Omit<TableType, TableCommonToOmitType>;
}