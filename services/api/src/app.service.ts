import {Injectable} from '@nestjs/common';
import DB from './lib/db';
import Logger from './lib/logger';
import {ServerCantAccessToDatabase} from './lib/exceptions/custom-http-exceptions/server-cant-access-to-database.exception';

@Injectable()
export class AppService {
    async getHealthiness(): Promise<string> {

        try {
            await DB.ping();
        } catch (error) {
            const errorDescription = 'Can\'t ping DB during health check';
            Logger.server.error(error, {errorDescription});
            throw new ServerCantAccessToDatabase();
        }

        return 'healthy';
    }
}
