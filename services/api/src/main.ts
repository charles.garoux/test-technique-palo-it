import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import DB from './lib/db';
import {DocumentBuilder, OpenAPIObject, SwaggerDocumentOptions, SwaggerModule} from '@nestjs/swagger';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    /**
     * Init Database connection
     */
    await DB.init(
        process.env.DB_HOST || 'localhost',
        process.env.DB_USER || 'toto',
        process.env.DB_PASSWORD || '1234',
        process.env.DB_DATABASE || 'example',
        parseInt(process.env.DB_CLIENT_POOL_TIMEOUT || '10000', 10),
        parseInt(process.env.DB_CLIENT_CONNECTION_TIMEOUT || '10000', 10)
    );

    /**
     * Init API Documentation
     */
    const config = new DocumentBuilder()
        .setTitle('Station Project')
        .setDescription('The "Station Project" API description')
        .setVersion(require('../package.json').version)
        .build();
    const options: SwaggerDocumentOptions = {
        operationIdFactory: (controllerKey: string, methodKey: string) => methodKey
    };
    const document: OpenAPIObject = SwaggerModule.createDocument(app, config, options);
    SwaggerModule.setup('doc', app, document);

    /**
     * Init HTTP serveur
     */
    await app.listen(3000);
}

bootstrap();
