import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import generateResourceController from './lib/nest/controller-resource-generator';
import DealerTableInterface from './data-structures/db/dealer-table.interface';
import StationTableInterface from './data-structures/db/station-table.interface';
import {LoggerInsertLogDataMiddleware, LoggerRequestMiddleware} from './lib/logger/express-middleware';
import * as httpContext from 'express-http-context';
import Dealer from './data-structures/db/dealer.dto';
import Station from './data-structures/db/station.dto';

@Module({
    imports: [],
    controllers: [AppController,
        generateResourceController<DealerTableInterface>('dealers', Dealer),
        generateResourceController<StationTableInterface>('stations', Station)
    ],
    providers: [AppService]
})

export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(
                // Log request payload
                LoggerRequestMiddleware,
                // Store request data
                httpContext.middleware, LoggerInsertLogDataMiddleware
            )
            .forRoutes('/');
    }
}
