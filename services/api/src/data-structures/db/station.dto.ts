import {TableCommonToOmitType} from './table-common-base.interface';
import StationTableInterface, {StationIdType} from './station-table.interface';

class Station implements Omit<StationTableInterface, TableCommonToOmitType> {
    id: StationIdType;
    address: string;
    city: string;
    country_code: string;
    dealer_id: number;
    latitude: number;
    longitude: number;
    postal_code: string;
    station_name: string;
}

export default Station;
