import TableCommonBaseInterface from './table-common-base.interface';

export type StationIdType = number

interface StationTableInterface extends TableCommonBaseInterface {
    id: StationIdType

    country_code: string
    station_name: string
    latitude: number
    longitude: number
    address: string
    postal_code: string
    dealer_id: number
    city: string
}

export default StationTableInterface;
