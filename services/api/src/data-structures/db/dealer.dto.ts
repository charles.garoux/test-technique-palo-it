import {TableCommonToOmitType} from './table-common-base.interface';
import DealerTableInterface, {DealerIdType} from './dealer-table.interface';

class Dealer implements Omit<DealerTableInterface, TableCommonToOmitType> {
    id: DealerIdType;
    name: string;
    dealer_type: number;
}

export default Dealer;
