import TableCommonBaseInterface from './table-common-base.interface';

export type DealerIdType = number

interface DealerTableInterface extends TableCommonBaseInterface {
    id: DealerIdType

    name: string
    dealer_type: number
}

export default DealerTableInterface;
