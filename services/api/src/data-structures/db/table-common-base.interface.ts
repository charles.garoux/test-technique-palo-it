type TimestampType = number

interface TableCommonBaseInterface {
    id: number
    modification_time: TimestampType
    insertion_time: TimestampType
}

export type TableCommonToOmitType = 'modification_time' | 'insertion_time';

export default TableCommonBaseInterface;