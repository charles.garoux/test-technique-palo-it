import {Controller, Get, HttpException, InternalServerErrorException} from '@nestjs/common';
import {AppService} from './app.service';
import {ApiResponse} from '@nestjs/swagger';
import {CustomHttpStatus} from './lib/exceptions/custom-http-exceptions/custom-http-status';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get('healthcheck')
    @ApiResponse({
        status: CustomHttpStatus.SERVER_CANT_ACCESS_TO_DATABASE,
        description: 'Server can\'t access to DB'
    })
    async healthCheck(): Promise<string> {
        try {
            return this.appService.getHealthiness();
        } catch (error) {
            if (error instanceof HttpException)
                throw error;
            else
                new InternalServerErrorException();
        }
    }
}
