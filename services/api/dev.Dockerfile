FROM node:16.13-alpine3.12
ENV LOG_DIRECTORY=/var/log/app

# Install "dumb-init"
RUN apk update && \
    apk upgrade && \
    apk --no-cache add dumb-init \
    && rm -rf /var/lib/apt/lists/*

# Setup log directory
WORKDIR /var/log/app
RUN chown -R node:node .

# Setup app directory
WORKDIR /app
COPY --chown=node:node . .
USER node

EXPOSE 3000

HEALTHCHECK --start-period=5s --interval=1m --timeout=30s --retries=3 CMD npm run dev:healthcheck

ENTRYPOINT ["dumb-init", "npm", "run", "dev:docker"]
