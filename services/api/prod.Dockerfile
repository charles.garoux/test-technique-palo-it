## Builder Stage =============================
FROM node:16.13-alpine3.12 AS builder

# Setup build directory
WORKDIR /usr/app
RUN chown node:node .
COPY --chown=node:node . .

USER node

# Build project
RUN npm install
RUN npm run build

## Runner Stage =============================
FROM node:16.13-alpine3.12 as prodduction
ENV NODE_ENV=production
ENV LOG_DIRECTORY=/var/log/app

# Install "dumb-init"
RUN apk update && \
    apk upgrade && \
    apk --no-cache add dumb-init

# Setup log directory
RUN mkdir -p $LOG_DIRECTORY && \
    chown -R node:node $LOG_DIRECTORY

# Setup app directory
WORKDIR /usr/app
COPY package*.json ./
RUN chown -R node:node .

USER node
RUN npm install --only=production
COPY --chown=node:node --from=builder /usr/app/dist dist/

HEALTHCHECK --start-period=5s --interval=1m --timeout=30s --retries=3 CMD npm run dev:healthcheck

ENTRYPOINT ["dumb-init"]
CMD ["npm", "run", "start:prod"]
