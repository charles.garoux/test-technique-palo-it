# API

**Présentation** :
    Serveur HTTP servant d’API pour accéder en lecture aux données du projet stockées en BDD.


## Variables d'environnements

### Base de données

* **DB_HOST** = Adresse de l'hôte du serveur MariaDB
* **DB_USER** = Nom de l'utilisateur de l'API pour accéder à la base de données
* **DB_PASSWORD** = Mot de passe de l'utilisateur de l'API pour accéder à la base de données
* **DB_DATABASE** = Nom de la base de données de l'API sur le serveur MariaDB
* **DB_CLIENT_POOL_TIMEOUT** = Temps d'attente (en millisecondes) avant l'abandon de l'acquisition d'une "pool" de connexion avec le serveur MariaDB
* **DB_CLIENT_CONNECTION_TIMEOUT** = Temps d'attente (en millisecondes) avant l'abandon d'initialisation d'une connexion au serveur MariaDB

### Logs

* **LOG_LEVEL** = Niveau d'affichage des logs
(Possibilités dans l'ordre d'importance : `error`,`warn`,`info`,`http`,`verbose`,`debug` ou `silly`)
(Voir les niveaux de log de **Winston**: https://www.npmjs.com/package/winston#logging-levels)

* **LOG_DIRECTORY** = Dossier où seront écrit les fichiers de logs

## Autre

**Stack technique** :
* Node.js 16.x (LTS)
* Typescript
* Nest.js (Express.js)
* Docker
