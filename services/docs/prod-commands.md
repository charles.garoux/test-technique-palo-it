# Commandes en environnement de production

**Lancer l'exécution de l’infrastructure de production** :
 ```shell script
bash compose.bash prod up -d
 ```

**Couper l'exécution de l’infrastructure de production** :
 ```shell script
bash compose.bash prod down
 ```

**Utiliser le service `tool`, exemple avec la table `dealers`** :
```shell script
bash tool-exec.bash npm start STDIN dealers dealer_id < ../subject/data/dealers.csv
```
> **Attention** : le CSV n’est pas déployé avec le code source. Il est nécessaire de l'apporter sur le serveur
> (par exemple : via la commande `scp`) ou un moyen détourné par le réseau.

**Utiliser le service `tool`, exemple avec la table `stations`** :
```shell script
bash tool-exec.bash npm start STDIN stations station_id < ../subject/data/stations.csv
```
> **Attention** : le CSV n’est pas déployé avec le code source. Il est nécessaire de l'apporter sur le serveur
> (par exemple : via la commande `scp`) ou un moyen détourné par le réseau.
