# Commandes pour lancer 

## Avec docker-compose

### L'infrastructure

**Lancer l'exécution de l’infrastructure de production** :
 ```shell script
docker-compose --env-file docker-compose.env -p chgaroux-tech-test -f docker-compose.base.yaml -f docker-compose.EXEC_ENV.yaml up
```
> EXEC_ENV = environnement d'exécution (possibilité : "**prod**" et "**dev**")

### Le service `tool`

**Utiliser le service `tool`** :
```shell script
docker-compose --env-file docker-compose.env -p chgaroux-tech-test -f docker-compose.base.yaml -f docker-compose.EXEC_ENV.yaml \
exec -T \
tool npm start PATH_TO_CSV TABLE_NAME COLUMN_ID_NAME CSV_SEPARATOR
```
> EXEC_ENV = environnement d'exécution (possibilité : "**prod**" et "**dev**")
>
> PATH_TO_CSV = chemin local vers le CSV ou valeur special ;
> * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
>
> TABLE_NAME = nom de table en BDD
>
> COLUMN_ID_NAME = nom de la colonne qui contient l'identifiant unique de l'élément
>
> CSV_SEPARATOR = séparateur du CSV (exemples : "**,**", "**;**", ...)

**Utiliser le service `tool` avec lecture sur STDIN** :
```shell script
docker-compose --env-file docker-compose.env -p chgaroux-tech-test -f docker-compose.base.yaml -f docker-compose.EXEC_ENV.yaml \
exec -T \
tool npm start STDIN TABLE_NAME COLUMN_ID_NAME CSV_SEPARATOR < PATH_TO_CSV
```
> PATH_TO_CSV = chemin local vers le CSV ou valeur special ;
> * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
>
> TABLE_NAME = nom de table en BDD
>
> COLUMN_ID_NAME = nom de la colonne qui contient l'identifiant unique de l'élément
>
> CSV_SEPARATOR = séparateur du CSV (exemples : "**,**", "**;**", ...)

## Sans docker-compose

**Lancer l'exécution de l’infrastructure de production** :
```shell script
bash compose.bash EXEC_ENV COMPOSE_COMMAND FLAGS
```
> EXEC_ENV = environnement d'exécution (possibilité : "**prod**" et "**dev**")
>
> COMPOSE_COMMAND = commande docker-compose (exemples : "**up**", "**down**", ...)
>
> FLAGS = (**optionnel**) flags de commande docker compose (exemple pour la commande "**up**": "**-d**")

**Utiliser le service `tool`** :
```shell script
bash tool-exec.bash npm start PATH_TO_CSV TABLE_NAME COLUMN_ID_NAME CSV_SEPARATOR
```
> PATH_TO_CSV = chemin local vers le CSV ou valeur special ;
> * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
>
> TABLE_NAME = nom de table en BDD
>
> COLUMN_ID_NAME = nom de la colonne qui contient l'identifiant unique de l'élément
>
> CSV_SEPARATOR = séparateur du CSV (exemples : "**,**", "**;**", ...)

**Utiliser le service `tool` avec lecture sur STDIN** :
```shell script
bash tool-exec.bash npm start STDIN TABLE_NAME COLUMN_ID_NAME CSV_SEPARATOR < PATH_TO_CSV
```
> PATH_TO_CSV = chemin local vers le CSV ou valeur special ;
> * **STDIN** : la lecture du CSV se fera sur l'entrée standard, à la place de lire un fichier
>
> TABLE_NAME = nom de table en BDD
>
> COLUMN_ID_NAME = nom de la colonne qui contient l'identifiant unique de l'élément
>
> CSV_SEPARATOR = séparateur du CSV (exemples : "**,**", "**;**", ...)

