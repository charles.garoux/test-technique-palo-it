# Commandes en environnement de développement

**Lancer l'exécution de l’infrastructure de développement** :
 ```shell script
bash compose.bash dev up -d
 ```

**Couper l'exécution de l’infrastructure de développement** :
 ```shell script
bash compose.bash dev down
 ```

**Utiliser le service `tool`, exemple avec la table `dealers`** :
```shell script
bash tool-exec.bash npm start STDIN dealers dealer_id < ../subject/data/dealers.csv
```

**Utiliser le service `tool`, exemple avec la table `stations`** :
```shell script
bash tool-exec.bash npm start STDIN stations station_id < ../subject/data/stations.csv
```