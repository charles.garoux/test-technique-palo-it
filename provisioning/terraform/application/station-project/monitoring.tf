module "operator_notification_channels" {
  source = "../../modules/notification-channels"
}

# CPU

module "station_server_cpu_alert" {
  source = "../../modules/vm-alert"

  alert_name = "CPU alert for ${module.server.server_instance_name} VM"
  notification_channel_name_list = module.operator_notification_channels.notification_channel_name_list

  instance_name = module.server.server_instance_name
  threshold_value = 0.8
  duration_in_minutes = 1
  compute_metric_type = "instance/cpu/utilization"
}

locals {
  service_uptime_check_list = {
    "API" = {
      check_name = "HTTP uptime for API health check"
      alert_name = "HTTP uptime alert for API health check"
      port = 3000
      path = "/healthcheck"
      enable_content_matching = true
      response_content = "OK"
      response_matcher = "CONTAINS_STRING"
    }

    "Kibana" = {
      check_name = "HTTP uptime for Kibana health check"
      alert_name = "HTTP uptime alert for Kibana health check"
      port = 5601
      path = "/api/task_manager/_health"
    }

    "Elasticsearch" = {
      check_name = "HTTP uptime for Elasticsearch health check"
      alert_name = "HTTP uptime alert for Elasticsearch health check"
      port = 9200
      path = "/_cluster/health"
    },
  }
}

module "station_service_http_uptime_check" {
  source = "../../modules/http-uptime-check"
  for_each = local.service_uptime_check_list

  check_name = each.value.check_name
  alert_name = each.value.check_name

  notification_channel_name_list = module.operator_notification_channels.notification_channel_name_list
  gcp_project_id = var.gcp_project

  host = module.server.server_public_address
  port = each.value.port
  path = each.value.path

  check_period_in_minute = 1

  enable_content_matching = try(each.value.enable_content_matching, false)
  response_content = try(each.value.response_content, "")
  response_matcher = try(each.value.response_matcher, "CONTAINS_STRING")
}