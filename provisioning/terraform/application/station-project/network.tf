module "network" {
  source = "../../modules/network"
  depends_on = [
    module.enable_gcp_service
  ]

  name = local.application_name
  region = module.var_process.cloud_region
  zone = local.cloud_zone

  network_content_short_description = "project server"
  open_ports = ["22", "3000", "9200", "5601"]
}