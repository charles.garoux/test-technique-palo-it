module "server" {
  source = "../../station-project-modules/station-project-server"

  application_name = local.application_name
  cloud_region = local.cloud_region
  cloud_zone = local.cloud_zone
  instance_type = local.instance_type

  vpc_name = module.network.vpc_name
}
