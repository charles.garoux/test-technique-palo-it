provider "google" {
  credentials = file(var.gcp_credentials_file_path)
  project = var.gcp_project
}

module "var_process" {
  source = "../../modules/variables-processor"
  vm_size = var.vm_size
  world_region = var.world_region
}

locals {
  application_name = "station-project"
  cloud_region = module.var_process.cloud_region
  cloud_zone = data.google_compute_zones.available.names[0]
  instance_type = module.var_process.instance_type
}

module "enable_gcp_service" {
  source = "../../modules/enable-gcp-service"
  sub_domaine_service_name_set = [
    "compute"
  ]
}

data "google_compute_zones" "available" {
  depends_on = [
    module.enable_gcp_service
  ]

  region = local.cloud_region
}