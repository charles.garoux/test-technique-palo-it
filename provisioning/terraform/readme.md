# Configuration Terraform

**Présentation** :
	Déploiement de l'infrastructure Cloud sur Google Cloud Platform des services du projet et déploiement des services.

## Les pré-requis (Déploiement & Développement)

1. Installer les outils nécessaires au déploiement :
    * La CLI `terraform` 1.0.11
    * La commande `jq` (https://stedolan.github.io/jq/)
    * La commande `md5sum`

2. Action à faire avec le projet :
    * **Activer le service "Cloud Resource Management"** manuellement dans la **console GCP** : https://console.cloud.google.com/apis/api/cloudresourcemanager.googleapis.com/overview
    * **Lier un "Billing Account"** au projet manuellement dans la **console GCP** : https://console.cloud.google.com/billing/linkedaccount

3. Récupérer les données requises pour la configuration du déploiement :
   * **Identifiant du projet GCP**
   * Le fichier **JSON d’identifiant de compte de service GCP** pour Terraform (plus de details dans [docs/credentials.md](./docs/credentials.md))

4. Placer, dans le répertoire `credentials/gcp/`, le fichier JSON d’identifiant de "Service Account" de GCP (plus de details dans [docs/credentials.md](./docs/credentials.md))

5. **Configurer le déploiement** avec les informations nécessaires à Terraform, copier le fichier `terraform.tfvars.example` en `terraform.tfvars` puis remplacer les valeurs des variables par celles correspondantes aux informations de l'étape 3 :
    * `gcp_project` = identifiant du project Google Cloud Platform
    * `gcp_credentials_file_path` = chemin vers le fichier JSON d'identification

6. Retirer les dossiers des services pouvant poser problème au déploiement (volumes Docker, node_modules, …), à partir de la racine du dépôt :
   * `services/docker-volumes`
   * `services/api/node_modules`
   * `services/api/dist`
   * `services/api/logs`
   * `services/tool/node_modules`
   * `services/tool/logs`

## Déploiement

**Contexte d'exécution** :
    Les commandes nécessitent d'être exécuté depuis le même dossier que ce document (sauf indication contraire).

```shell script
terraform init
```
> Initialise le projet Terraform : télécharge les modules et providers externe et enregistrer les liens interne.

```shell script
terraform plan
```
> Affiche les modifications qui seront appliquées à l'infrastructure actuelle.

```shell script
terraform apply
```
> Applique les modifications sur l’infrastructure actuelle.

## Autre

**Autre documentation** :
    D’autre documentation pouvant être utile sont présentes dans le dossier [docs](./docs)

**Stack technique** :
* **Terraform** 1.0.11 - Outil de déploiement
* **Google Cloud Platform** - Plateforme Cloud