variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "open_ports" {
  type = list(string)
  description = "List of ingress open ports"
  default = [
    "22",
    "3000",
    "9200"
  ]
}

variable "network_content_short_description" {
  type = string
  default = null
}