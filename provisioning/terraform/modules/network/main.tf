resource "google_compute_network" "vpc_network" {
  name = "${var.name}-vpc"
  description = (var.network_content_short_description != null)? "VPC for: ${var.network_content_short_description}" : null
}

resource "google_compute_firewall" "input_firewall" {
  name = "${var.name}-ingress-firewall"
  network = google_compute_network.vpc_network.name

  direction = "INGRESS"
  source_ranges = [
    "0.0.0.0/0"
  ]

  allow {
    protocol = "tcp"
    ports = var.open_ports
  }
}
