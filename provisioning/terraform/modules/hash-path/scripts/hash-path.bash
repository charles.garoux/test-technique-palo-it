#!/usr/bin/env bash

# Input : read JSON on STDIN with format {path: string}
# Output : write JSON on STDOUT with format {hash: string}
#
# Dependency :
# * jq
# * md5sum

set -ueo pipefail
QUERY_PATH=$(cat | jq -r '.path')
HASH=$(tar -cf - "$QUERY_PATH" | md5sum | cut -d' ' -f1)
printf "{\\\"hash\\\":\\\"$HASH\\\"}"