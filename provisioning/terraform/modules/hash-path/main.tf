# Original idea : https://github.com/hashicorp/terraform/issues/6065#issuecomment-281347100

variable "path" {
  type = string
  description = "Path to file (or directory) to hash"
}

data "external" "hash_path" {
  program = ["bash", "${path.module}/scripts/hash-path.bash"]
  query = { path = var.path }
}

output "hash" {
  value = data.external.hash_path.result.hash
}