variable "sub_domaine_service_name_set" {
  type = set(string)
  description = "The short name of the service API (Examples: Service = \"Cloud Pub / Sub API\" => Service name = \"pubsub.googleapis.com\" => sub_domain_service_name = \"pubsub\", here \"pubsub\" is the expected value)"
}

variable "disable_on_destroy" {
  default = false
  description = "Disable the service when the terraform resource is destroyed"
}