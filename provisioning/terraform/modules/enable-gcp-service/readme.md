# Enable GCP service

**GCP Prerequisites** :
    Activate the "Cloud Resource Management" service manually in the **GCP console**: https://console.cloud.google.com/apis/api/cloudresourcemanager.googleapis.com/overview