resource "google_project_service" "enabled_service" {
  for_each = var.sub_domaine_service_name_set
  service = "${each.value}.googleapis.com"

  disable_on_destroy = false
}