locals {
  email_map = merge(
    [for email_map_JSON in var.project_relative_path_to_operators_emails :
    jsondecode(file("${path.root}/${email_map_JSON}"))]...
  )
}

resource "google_monitoring_notification_channel" "operator_channel" {
  for_each = local.email_map
  display_name = "${each.key} email"
  type = "email"
  labels = {
    email_address = each.value
  }
}