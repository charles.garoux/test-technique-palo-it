output "notification_channel_name_list" {
  value = [for channel in google_monitoring_notification_channel.operator_channel: channel.name]
}