# Alert

variable "alert_name" {
  type = string
}

variable "instance_name" {
  type = string
}

variable "compute_metric_type" {
  type = string
  description = "Exemple instance/cpu/utilization"
}

variable "threshold_value" {
  type = number
  default = 0.8
  validation {
    condition = var.threshold_value > 0 &&  var.threshold_value <= 1
    error_message = "The threshold_value value must be between 0 and 1."
  }
  description = "Threshold value (floating number between 0 and 1) before alerting"
}

variable "duration_in_minutes" {
  type = number
  default = 1
  description = "Duration (in minutes) above a threshold before alerting"
}

# Notification

variable "notification_channel_name_list" {
  type = list(string)
}