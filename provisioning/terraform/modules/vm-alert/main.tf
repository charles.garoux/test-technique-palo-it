resource "google_monitoring_alert_policy" "alert_policy" {
  display_name = var.alert_name

  notification_channels = var.notification_channel_name_list

  combiner = "OR"
  conditions {
    display_name = var.alert_name
    condition_threshold {
      filter = "metric.type=\"compute.googleapis.com/${var.compute_metric_type}\" resource.type=\"gce_instance\" metric.label.\"instance_name\"=\"${var.instance_name}\""
      threshold_value = var.threshold_value
      duration = "${var.duration_in_minutes * 60}s"
      comparison = "COMPARISON_GT"

      trigger {
        count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
      }
    }
  }
}