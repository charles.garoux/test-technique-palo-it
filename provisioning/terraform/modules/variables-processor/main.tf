data "external" "config_data_JSON" {
  for_each = var.path_to_JSON_config_data_map
  program = ["cat", "${path.root}${each.value}"]
}

locals {
  instance_type = data.external.config_data_JSON["instance_types"].result[lower(var.vm_size)]
  cloud_region = data.external.config_data_JSON["regions"].result[upper(var.world_region)]
}