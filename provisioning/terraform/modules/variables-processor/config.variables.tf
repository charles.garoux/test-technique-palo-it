variable "path_to_JSON_config_data_map" {
  type = map(string)
  default = {
    regions = "/config-data/GCP/region/world-regions-map.json"
    instance_types = "/config-data/GCP/vm-instances/instance-types-map.json"
  }
  description = "Map of path to JSON with data"
  # Warning: the expression "${path.root}" will be added to the path prefix and the file must be a JSON
}
