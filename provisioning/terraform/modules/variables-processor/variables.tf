variable "vm_size" {
  type        = string
  description = "The size of the instances (example: small, medium, ...)"
}

variable "world_region" {
  type        = string
  description = "The simplified name of the world region (example: EU, US, AS, ...)"
}

# To see or modify the values linking the input values (world region, size or type of machine, ...)
# and the environment variables (Cloud location, type of instance, ...): see ./config.variables.tf
