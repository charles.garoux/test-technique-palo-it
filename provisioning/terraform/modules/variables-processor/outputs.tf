output "instance_type" {
  value       = local.instance_type
  description = "Cloud provider VM instance type"
}

output "cloud_region" {
  value       = local.cloud_region
  description = "Cloud provider region name"
}