locals {
  content_matchers = (var.enable_content_matching == true)? [
    {
      content = var.response_content
      matcher = var.response_matcher
    }] : []
}

resource "google_monitoring_uptime_check_config" "http_check" {
  display_name = var.check_name
  period = "${var.check_period_in_minute * 60}s"
  timeout = "${var.check_timeout}s"

  http_check {
    path = var.path
    port = var.port
    request_method = var.request_method
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      project_id = var.gcp_project_id
      host = var.host
    }
  }

  dynamic "content_matchers" {
    for_each = local.content_matchers
    content {
      content = content_matchers.value["content"]
      matcher = content_matchers.value["matcher"]
    }
  }
}

resource "google_monitoring_alert_policy" "http_check_alert_policy" {
  display_name = var.alert_name

  notification_channels = var.notification_channel_name_list

  combiner = "OR"
  conditions {
    display_name = var.alert_name
    condition_threshold {
      filter = "metric.type=\"monitoring.googleapis.com/uptime_check/check_passed\" resource.type=\"uptime_url\" metric.label.\"check_id\"=\"${google_monitoring_uptime_check_config.http_check.uptime_check_id}\""
      duration = "${var.duration_in_minutes * 60}s"

      threshold_value = 1
      comparison = "COMPARISON_LT"

      trigger {
        count = 1
      }

      aggregations {
        alignment_period = "60s"
        cross_series_reducer = "REDUCE_COUNT_TRUE"
        group_by_fields = [
          "metric.label.checker_location"
        ]
        per_series_aligner = "ALIGN_NEXT_OLDER"
      }
    }
  }
}