# Uptime check

variable "gcp_project_id" {
  type = string
}

variable "check_name" {
  type = string
}

variable "host" {
  type = string
  description = "Host public address to check"
}

variable "port" {
  type = number
  default = 80
  description = "Port to check"
}

variable "path" {
  type = string
  default = "/"
}

variable "request_method" {
  type = string
  default = "GET"
  validation {
    condition = contains([
      "METHOD_UNSPECIFIED",
      "GET",
      "POST"
    ], var.request_method)
    error_message = "Possible values are only : METHOD_UNSPECIFIED, GET, and POST."
  }
}

variable "check_period_in_minute" {
  type = number
  default = 1
  description = "Number of minutes between each check"

  validation {
    condition = contains([
      1,
      5,
      10,
      15
    ], var.check_period_in_minute)
    error_message = "Possible values are only : 1, 5, 10, and 15."
  }
}

variable "check_timeout" {
  type = number
  default = 1
  description = "Number of seconds before check fail"

  validation {
    condition = var.check_timeout >= 1 && var.check_timeout <=60
    error_message = "Must be between 1 and 60 seconds."
  }
}

variable "enable_content_matching" {
  type = bool
  default = false
  description = "Enable response validation with response content matching"
}

variable "response_content" {
  type = string
  default = ""
  description = "String or regex content to match (max 1024 bytes)"
}

variable "response_matcher" {
  type = string
  default = "CONTAINS_STRING"
  description = "The type of content matcher that will be applied to the server output, compared to the content string when the check is run. Default value is CONTAINS_STRING. Possible values are CONTAINS_STRING, NOT_CONTAINS_STRING, MATCHES_REGEX, and NOT_MATCHES_REGEX"

  validation {
    condition = contains([
      "CONTAINS_STRING",
      "NOT_CONTAINS_STRING",
      "MATCHES_REGEX",
      "NOT_MATCHES_REGEX"
    ], var.response_matcher)
    error_message = "Possible values are only : CONTAINS_STRING, NOT_CONTAINS_STRING, MATCHES_REGEX, and NOT_MATCHES_REGEX."
  }
}

# Alert

variable "alert_name" {
  type = string
}

variable "duration_in_minutes" {
  type = number
  default = 1
  description = "Duration (in minutes) above a threshold before alerting"
}

# Notification

variable "notification_channel_name_list" {
  type = list(string)
}