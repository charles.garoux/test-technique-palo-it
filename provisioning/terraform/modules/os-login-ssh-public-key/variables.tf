variable "path_to_ssh_public_key" {
  type = string
  default = "~/.ssh/id_rsa.pub"
}