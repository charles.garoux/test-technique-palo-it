output "instance_template_id" {
  value = google_compute_instance_template.vm_template.id
  description = "Id use to create resource like google_compute_instance_from_template"
}