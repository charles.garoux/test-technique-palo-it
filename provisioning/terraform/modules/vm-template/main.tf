resource "google_compute_instance_template" "vm_template" {
  name = "${var.name}-vm-template"
  machine_type = var.machine_type

  network_interface {
    network = var.network_name
  }
  can_ip_forward = false

  scheduling {
    automatic_restart = (var.preemptible == false)? true : false
    on_host_maintenance = (var.preemptible == false)? "MIGRATE" : "TERMINATE"
    preemptible = var.preemptible
  }

  // Use an existing disk resource
  disk {
    source = google_compute_disk.disk.name
    auto_delete = false
    boot = true
  }

  metadata = {
    enable-oslogin: "TRUE" // Todo : verifier l'interet
  }
}

data "google_compute_image" "image" {
  family = "cos-93-lts"
  project = "cos-cloud"
}

resource "google_compute_disk" "disk" {
  name = "${var.name}-existing-disk"
  image = data.google_compute_image.image.self_link
  size = var.disk_size
  type = "pd-ssd"
  zone = var.zone
}
