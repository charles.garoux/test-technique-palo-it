variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

# VM

variable "machine_type" {
  type = string
}

variable "preemptible" {
  type = bool
  default = false
}

variable "network_name" {
  type = string
}

# disk

variable "disk_size" {
  type = number
  default = 10
}
