# Commandes pratique

**Pré-requis** :
* Avoir l'outil `jq` installé (https://stedolan.github.io/jq/)

**Se connecter en SSH au serveur** (avec l'outil `jq`), après déploiement par Terraform ,depuis la racine de la configuration Terraform :
```shell script
ssh charles_garoux@$(cat terraform.tfstate | jq .outputs.server_public_address.value -r)
```

**Envoyer de fichier au serveur par SCP** (avec l'outil `jq`), après déploiement par Terraform ,depuis la racine de la configuration Terraform :
 ```shell script
scp PATH_TO_LOCAL_FILE charles_garoux@$(cat terraform.tfstate | jq .outputs.server_public_address.value -r):PATH_TO_REMOTE_FILE
 ```

> PATH_TO_LOCAL_FILE = Chemin vers le fichier local (source)
>
> PATH_TO_REMOTE_FILE = Chemin vers le fichier distant (destination)