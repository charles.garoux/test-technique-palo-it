# Modification

**Surveillance par Terraform** :
    La configuration Terraform calcule l'empreinte, l'hash, de plusieurs éléments indépendants à surveiller et à mettre à jour en cas de modification :
* Le dossier /services/ complet
* Le CSV /subject/data/dealers.csv
* Le CSV /subject/data/stations.csv

## Modifier les services

**Modification de service** :
    Si un élément dans le dossier `/services` change, la planification de Terraform proposera le redéploiement des services.
    L'orchestration par Docker Compose va gérer l’application des modifications des services.

## Modifier les données (par CSV)

**Modification de CSV** :
    Si un des CSV est modifié, la planification de Terraform proposera son application sur l'infrastructure.
    L’application d’un CSV se fait avec le service "tool" (voir [/services/tool/readme.md](/services/tool/readme.md)).
