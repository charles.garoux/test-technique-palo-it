# Credentials

## Service Account Google Cloud Platform

**Format du fichier** : JSON (généré par GCP)

**Rôles du Service Account** :
* Compute Admin
* Monitoring Admin

**Le fichier** :
    Placer le fichier de **clé JSON** du Service Account dans le dossier [../credentials/gcp](../credentials/gcp) avec l'extension `.secret.json`.

> Les fichiers en `*.secret.*` sont ignorés par Git
