# Développement

## Outils nécessaire

- **Terraform** : https://www.terraform.io/downloads.html

### Version d'outil

**Terraform** :
    La version 1.0.11 a été utilisée pour le développement, les versions supérieures sont rétro-compatibles donc utilisables.
    
> Attention : Il est déconseillé d’utiliser plusieurs versions différentes de Terraform sur un même "remote state" (ou "workspace") pour éviter des incompatibilités.

## L'architecture du projet

### /modules

Ce répertoire contient tous modules Terraform réutilisable dans d'autres projets.

**Exemples** :
* VPC avec pare-feu
* Surveillance d'accessibilité par HTTP avec alerte
* ...

>**Attention** : modifier un module peut impacter toutes les autres configurations qui l'utilisent.

>**Recommandation** :
> * si la modification est l’ajout de variable, ajouter à la variable la valeur actuelle en valeur par défaut pour ne pas impacter les configurations n’utilisant pas cette variable
> * si la modification est majeure, il peut être intéressant d'étudier le fait d’en faire un nouveau module ou sous-module

### /station-project-modules

Ce répertoire contient tous les modules Terraform utilisable seulement dans le projet Terraform de "Station project".

**Exemples** :
* Serveur avec déploiement des services
* ...

### /config-data

Répertoire qui contient les données utilisées pour la configuration de l’infrastructure :
* GCP :
  * Les "notification channels" utilisé par le projet
  * Les régions
  * Les types d’instance de VM


Le format est le JSON pour une manipulation automatisée par des scripts ou des outils. 

### **/config.variables.tf

Fichier de configuration Terraform en HCL, il est présent dans tous les modules Terraform qui ont besoin d’une configuration avant utilisation.
