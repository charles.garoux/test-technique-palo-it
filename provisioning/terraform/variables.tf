variable "gcp_project" {
  description = "The project id"
}

variable "gcp_credentials_file_path" {
  description = "Path to credentials in JSON"
}

variable "vm_size" {
  type = string
  default = "free"
}
variable "world_region" {
  type = string
  default = "free"
}

variable "preemtible" {
  default = false
}
