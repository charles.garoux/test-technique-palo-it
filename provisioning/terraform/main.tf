module "station_project" {
  source = "./application/station-project"

  world_region = "free"
  vm_size = "medium"
  preemtible = true
  gcp_credentials_file_path = var.gcp_credentials_file_path
  gcp_project = var.gcp_project
}