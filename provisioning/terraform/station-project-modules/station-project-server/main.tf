locals {
  user = "charles_garoux"
  server_name = "${var.application_name}-server"
}

module "vm_template" {
  source = "../../modules/vm-template"
  name = var.application_name
  machine_type = var.instance_type

  network_name = var.vpc_name

  region = var.cloud_region
  zone = var.cloud_zone
}

locals { # Upload file then exec command
  services_upload_and_run = {
    script_name = "start-services"
    path_to_source = "${path.root}/../../services"
    path_to_destination = "~/"
    commands = [
      "bash ~/services/compose.bash prod up --build -d"
    ]
  }

  data_injection_list = {
    "dealers" = {
      script_name = "inject-dealers"
      path_to_source = "${path.root}/../../subject/data/dealers.csv"
      path_to_destination = "~/dealers.csv"
      commands = [
        "bash ~/services/tool-exec.bash npm start STDIN dealers dealer_id < ~/dealers.csv",
        "rm ~/dealers.csv"
      ]
    }

    "stations" = {
      script_name = "inject-stations"
      path_to_source = "${path.root}/../../subject/data/stations.csv"
      path_to_destination = "~/stations.csv"
      commands = [
        "bash ~/services/tool-exec.bash npm start STDIN stations station_id < ~/stations.csv",
        "rm ~/stations.csv"
      ]
    }
  }

  remote_upload_exec_list = {
    "services" = local.services_upload_and_run
    "dealers" = local.data_injection_list.dealers
    "stations" = local.data_injection_list.stations
  }
}

resource "google_compute_instance_from_template" "station_server_instance" {
  name = local.server_name
  source_instance_template = module.vm_template.instance_template_id

  zone = var.cloud_zone

  network_interface {
    network = var.vpc_name
    access_config {
      network_tier = "PREMIUM"
    }
  }

  metadata = {
    ssh-keys: "${local.user}:${file("~/.ssh/id_rsa.pub")}"
  }

  metadata_startup_script = templatefile("${path.root}/scripts/startup-script.template.bash", {
    USERNAME = local.user
  })

  connection {
    type = "ssh"
    user = local.user
    private_key = file("~/.ssh/id_rsa")
    host = self.network_interface[0].access_config[0].nat_ip
    agent = false
    timeout = "60s"
  }

}

locals {
  server_host = google_compute_instance_from_template.station_server_instance.network_interface[0].access_config[0].nat_ip
  server_id = google_compute_instance_from_template.station_server_instance.instance_id
}

# ================== Upload and run ==================

module "hash_services" {
  for_each = local.remote_upload_exec_list

  source = "../../modules/hash-path"
  path = each.value["path_to_source"]
}

# ========= Services =========

resource "null_resource" "upload_and_start_services" {
  triggers = {
    host = local.server_host
    server_id = local.server_id
    hash = module.hash_services["services"].hash
  }

  connection {
    type = "ssh"
    user = local.user
    private_key = file("~/.ssh/id_rsa")
    host = local.server_host
    agent = false
    timeout = "60s"
  }

  # Services ================================

  provisioner "file" {
    source = local.remote_upload_exec_list.services["path_to_source"]
    destination = local.remote_upload_exec_list.services["path_to_destination"]
  }
  provisioner "remote-exec" {
    inline = local.remote_upload_exec_list.services["commands"]
    connection {
      script_path = "./tmp/${local.remote_upload_exec_list.services["script_name"]}.sh"
    }
  }
}

# ========= Data =========

resource "null_resource" "upload_and_push_dealers" {
  depends_on = [
    null_resource.upload_and_start_services
  ]

  triggers = {
    host = local.server_host
    server_id = local.server_id
    hash = module.hash_services["dealers"].hash
  }

  connection {
    type = "ssh"
    user = local.user
    private_key = file("~/.ssh/id_rsa")
    host = local.server_host
    agent = false
    timeout = "60s"
  }

  provisioner "file" {
    source = local.remote_upload_exec_list.dealers["path_to_source"]
    destination = local.remote_upload_exec_list.dealers["path_to_destination"]
  }
  provisioner "remote-exec" {
    inline = local.remote_upload_exec_list.dealers["commands"]
    connection {
      script_path = "./tmp/${local.remote_upload_exec_list.dealers["script_name"]}.sh"
    }
  }
}

resource "null_resource" "upload_and_push_station" {
  depends_on = [
    null_resource.upload_and_start_services,
    null_resource.upload_and_push_dealers
  ]

  triggers = {
    host = local.server_host
    server_id = local.server_id
    hash = module.hash_services["stations"].hash
  }

  connection {
    type = "ssh"
    user = local.user
    private_key = file("~/.ssh/id_rsa")
    host = local.server_host
    agent = false
    timeout = "60s"
  }

  provisioner "file" {
    source = local.remote_upload_exec_list.stations["path_to_source"]
    destination = local.remote_upload_exec_list.stations["path_to_destination"]
  }
  provisioner "remote-exec" {
    inline = local.remote_upload_exec_list.stations["commands"]
    connection {
      script_path = "./tmp/${local.remote_upload_exec_list.stations["script_name"]}.sh"
    }
  }
}