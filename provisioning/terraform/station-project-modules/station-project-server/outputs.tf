output "server_public_address" {
  value = local.server_host
}

output "server_instance_name" {
  value = google_compute_instance_from_template.station_server_instance.name
}