variable "application_name" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "vpc_name" {
  type = string
}

variable "cloud_region" {
  type = string
}

variable "cloud_zone" {
  type = string
}